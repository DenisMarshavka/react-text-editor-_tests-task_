import React from 'react'

import TextEditor from "./components/TextEditor/TextEditor"

const App = () => (
    <TextEditor />
);

export default App;
