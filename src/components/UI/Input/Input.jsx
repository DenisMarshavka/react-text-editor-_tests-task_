import React from 'react'
import s from './Input.module.css'

const Input = ({ type, autoFocus, onMouseDown, onBlur, inputRef, value, onTextChange, onKeyPress, style, id }) => (
    <input type={type} autoFocus={autoFocus} ref={inputRef} onBlur={onBlur} onKeyPress={onKeyPress} onMouseDown={onMouseDown} className={s.input} style={style} value={value} onChange={e => onTextChange(id, e.target.value)} />
);

export default Input
