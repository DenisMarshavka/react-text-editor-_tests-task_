import React from 'react'

import s from './BackDrop.module.css'

const BackDrop = ({ isActive, children, onChangeColorMode }) => (
    <div className={`${s.content} ${isActive ? s.active : ''}`} onClick={() => onChangeColorMode(false)}>
        { children }
    </div>
);

export default BackDrop
