import React from 'react'
import s from './Button.module.css'

const Button = ({ onClick, style, children }) => (
    <button className={s.button} style={style} onClick={onClick}>{ children }</button>
);

export default Button
