import React, {useState} from 'react'
import { CirclePicker } from 'react-color'

import s from './PanelControls.module.css'

import BackDrop from "../../UI/Backdrod/BackDrop"
import Input from "../../UI/Input/Input"
import Button from "../../UI/Button/Button";

const ControlsPanel = ({ onControlsButtonClick, onCompileToJson }) => {
    const [fonSize, setFontSize] = useState(16);
    const [colorSelected, setColorSelected] = useState('#6dce57');
    const [changeColorMode, setChangeColorMode] = useState(false);

    const handleFontSizeChange = size => {
      if ( !isNaN(+size) && (+size <= 110) ) setFontSize(+size);
    };

    const handleColorChange = color => setColorSelected(color.hex);

    return (
        <div className={s.wrapper}>
            <Button onClick={onControlsButtonClick.bind(this, 'background', colorSelected)}>Add background color</Button>
            <Button onClick={onControlsButtonClick.bind(this, 'color', colorSelected)}>Add font color</Button>

            <div className={s.controlFontSize}>
                <Input style={ {marginRight: 0, borderRight: 'none', borderRadius: 10, borderWidth: 2, textAlign: 'center', borderTopRightRadius: 0, borderBottomRightRadius: 0} } type="text" value={fonSize} onTextChange={handleFontSizeChange} />
                <Button style={ {borderTopLeftRadius: 0, borderBottomLeftRadius: 0} } onClick={onControlsButtonClick.bind(this, 'fontSize', fonSize)}>Change font size</Button>
            </div>

            <Button onClick={() => setChangeColorMode(true)}>Select color</Button>
            <Button onClick={onCompileToJson}>To Json</Button>

            <BackDrop isActive={changeColorMode} onChangeColorMode={setChangeColorMode}>
                <CirclePicker color={colorSelected} onChangeComplete={handleColorChange} />
            </BackDrop>
        </div>
    );
};

export default ControlsPanel
