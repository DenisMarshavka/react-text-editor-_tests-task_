import React, {useRef, useState} from 'react'
import {connect} from "react-redux"

import s from './TextContent.module.css'

import Input from "../../UI/Input/Input"
import {
    getUpdateChunk,
} from "../../../redux/actions/content"

const TextContent = React.memo(({ values, onTextSelect, onControlsButtonClick, onUpdateIdElementSelected, getUpdateChunk, onUpdateSelectionStart }) => {
    const [stateInputsValue, setStateInputsValue] = useState(values);
    const [indexesElementsSelectedText, setIndexesElementsSelectedText] = useState({start: null, end: null});

    const inputRef = useRef();

    const setTextSelected = () => {
        console.log(indexesElementsSelectedText);

        debugger;

        if (window.getSelection().toString().trim()) onTextSelect(window.getSelection().toString(), indexesElementsSelectedText.start, indexesElementsSelectedText.end);
    };

    const handleStartSelectedTextEvent = (event, id) => {
        let index = getArrayElementFindByParams(stateInputsValue, 'id', id).idElementSelected;

        console.log(event.type, index);
        setIndexesElementsSelectedText({
            ...indexesElementsSelectedText,
            start: index,
        });
    };

    const handleEndSelectedTextEvent = (event, id) => {
        let index = getArrayElementFindByParams(stateInputsValue, 'id', id).idElementSelected;

        console.log(event.type, index);
        setIndexesElementsSelectedText({
            ...indexesElementsSelectedText,
            end: index,
        });

        setTextSelected();
    };

    // const handleSelectedTextEvent = (event, id, isStartMouseEvent = false) => {
    //     if (isStartMouseEvent) {
    //         let index = getArrayElementFindByParams(stateInputsValue, 'id', id).idElementSelected;
    //
    //         console.log(event.type, index);
    //         setIndexesElementsSelectedText({
    //             ...indexesElementsSelectedText,
    //             start: index,
    //         });
    //     } else {
    //         let index = getArrayElementFindByParams(stateInputsValue, 'id', id).idElementSelected;
    //
    //         console.log(event.type, index);
    //         setIndexesElementsSelectedText({
    //             ...indexesElementsSelectedText,
    //             end: index,
    //         });
    //
    //         debugger;
    //         setTextSelected();
    //     }
    // };

    const getArrayElementFindByParams = (array, keyFinder, valueFinder) => {
        const copyArray = [...array],
              idElementSelected = copyArray.findIndex(e => e[keyFinder] === valueFinder);

        const result = {
            copyArray,
        };

        if (idElementSelected >= 0) result.idElementSelected = idElementSelected;
        if (copyArray[idElementSelected]) result.elementSelected = copyArray[idElementSelected];

        return result;
    };

    const handleTagToggle = id => {
        const copyArrayWithElementSelected = getArrayElementFindByParams(stateInputsValue, 'id', id);

        if (copyArrayWithElementSelected.elementSelected) {
            copyArrayWithElementSelected.elementSelected.isEditing = !copyArrayWithElementSelected.elementSelected.isEditing;
        }

        setStateInputsValue(copyArrayWithElementSelected.copyArray);
    };

    const handleValueChange = (id, text) => {
        const copyArrayWithElementSelected = getArrayElementFindByParams(stateInputsValue, 'id', id);
        let copyArray = copyArrayWithElementSelected.copyArray;

        copyArray[copyArrayWithElementSelected.idElementSelected].text = text;

        copyArray = copyArray.filter(e => e.text === null || e.text.trim());
        setStateInputsValue(copyArray);
    };

    const handleInputKeyPress = (id, event) => {
        if (event.key === 'Enter') {
            handleTagToggle(id);
            onUpdateIdElementSelected( getArrayElementFindByParams(stateInputsValue, 'id', id).idElementSelected );

            onControlsButtonClick('br', true);
        }
    };

    const handleInputBlur = elementId => {
        handleTagToggle(elementId);

        const copyArrayWithElementSelected = getArrayElementFindByParams(values, 'id', elementId);

        const copyArray = copyArrayWithElementSelected.copyArray,
              approveToElementsUpdate = copyArrayWithElementSelected.idElementSelected < 0 ||
                                        (copyArray[copyArrayWithElementSelected.idElementSelected] &&
                                            copyArray[copyArrayWithElementSelected.idElementSelected].text &&
                                                copyArray[copyArrayWithElementSelected.idElementSelected].text !== values[copyArrayWithElementSelected.idElementSelected].text);

        console.log(stateInputsValue);
        if (approveToElementsUpdate) getUpdateChunk(stateInputsValue);
    };

    let content = null;

    if (values && values.length) {
        content = values.map((e, i) => {
            let styles = {
                color: e.color || '#000',
                background: e.background || '#fff',
                fontSize: e.fontSize || 16,
            };

            if (!e.isEditing && e.background) {
                if (values[i - 1] && e.background === values[i - 1].background) {
                    styles.borderTopLeftRadius = 0;
                    styles.borderBottomLeftRadius = 0;

                    styles.paddingLeft = 0;
                }

                if (values[i + 1] && e.background === values[i + 1].background) {
                    styles.borderTopRightRadius = 0;
                    styles.borderBottomRightRadius = 0;

                    styles.paddingRight = 0;
                }
            }

            return e.isEditing ?
                <Input
                    inputRef={inputRef}
                    style={styles}
                    type="text" key={e.id + i} value={e.text || ''}
                    onTextChange={handleValueChange}
                    onTagToggle={handleTagToggle}
                    onBlur={() => handleInputBlur(e.id)}
                    autoFocus={true}
                    onMouseDown={() => onUpdateSelectionStart(inputRef.current.selectionStart)}
                    id={e.id}
                    onKeyPress={event => handleInputKeyPress(e.id, event)}
                /> :

                !e.br ?
                    <span
                        key={e.id + i}
                        className={`${e.background ? s.withBackground : ''}`}
                        style={styles}
                        onMouseDown={event => handleStartSelectedTextEvent(event, e.id)}
                        onMouseUp={event => handleEndSelectedTextEvent(event, e.id)}
                        onClick={handleTagToggle.bind(this, e.id)}
                    >{e.text}</span> :

                    <br key={e.id + i} />
        });
    }

    return (
        <div className={s.wrapper}>
            { content }
        </div>
    );
});

const mapDispatchToProps = dispatch => ({
    getUpdateChunk: newElement => dispatch(getUpdateChunk(newElement)),
});

export default connect(null, mapDispatchToProps)(TextContent)
