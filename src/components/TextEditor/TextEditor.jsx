import React from 'react'
import {connect} from "react-redux"

import {
    getUpdateContent,
    // setNewChunk
} from "../../redux/actions/content"
import PanelControls from "./PanelControls/PanelControls"
import TextContent from "./EditorBox/TextContent"

import s from './TextEditor.module.css'

const findSameLetters = ( chunkText, selectedText ) => {
    let allLettersChunk = chunkText.split(''),
        selectedLetters = selectedText.split(''),
        sameLetters = [];

    // debugger;

    // for (let la of selectedLetters) {
    //     for (let lc of allLettersChunk) {
    //         if (la === lc) {
    //             sameLetters.push(la);
    //             selectedText = selectedLetters.slice(1, selectedLetters.length);
    //             break;
    //         }
    //     }
    // }
    //
    // return sameLetters.join('');

    return selectedLetters.filter(e => {
        let resultWord = sameLetters.join('') + e;

        if (allLettersChunk.indexOf(e) >= 0 && chunkText.includes(resultWord)) {
            sameLetters.push(e);

            return true;
        }

        return false;
    }).join('');
};

class TextEditor extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            idElementSelected: 0,
            selectionStart: null,
            textSelected: '',
        }
    }

    handleUpdateSelectionStart = selectionStart => this.setState({ selectionStart});

    handleUpdateIdElementSelected = idElementSelected => {
        this.setState({ idElementSelected });
    };

    modifyChunks = (event, selectedValue) => {
        let startContentSelected = this.props.values.slice(this.state.idElementSelected,  this.props.values.length),
            existText = '',
            newContent = [];

        if (this.state.idElementSelected !== 0) {
            for (let i = 0; i < this.state.idElementSelected; i++) {
                newContent.push(this.props.values[i]);
            }
        }

        startContentSelected.forEach(v => {
            if (existText.trim() !== this.state.textSelected.trim()) {
                let restTextSelected = this.state.textSelected.indexOf(existText) > -1 ?
                    this.state.textSelected.slice(this.state.textSelected.indexOf(existText) + existText.length, this.state.textSelected.length) :
                    this.state.textSelected;

                if (v.text) {
                    if (restTextSelected.trim() === v.text.trim()) {
                        existText += v.text;

                        newContent.push({
                            ...v,
                            id: +new Date() + Math.random(),
                            text: v.text,
                            [event]: selectedValue,
                        });
                    } else {
                        let sameLetters = findSameLetters(v.text, restTextSelected);

                        existText += sameLetters;

                        let textSelected = this.state.textSelected.slice(this.state.textSelected.indexOf(sameLetters) + sameLetters.length, this.state.textSelected.length);

                        let firstWordChunk = v.text.slice(0, v.text.indexOf(sameLetters)),
                            lastWordChunk = v.text.slice(v.text.indexOf(sameLetters) + sameLetters.length, v.text.length);

                        this.setState({ textSelected });

                        if (sameLetters.trim()) {
                            if (firstWordChunk && firstWordChunk.length && lastWordChunk && lastWordChunk.length) {
                                newContent.push({
                                    ...v,
                                    id: +new Date() + Math.random(),
                                    text: firstWordChunk,
                                });

                                newContent.push({
                                    ...v,
                                    id: +new Date() + Math.random(),
                                    text: sameLetters,
                                    [event]: selectedValue,
                                });

                                newContent.push({
                                    ...v,
                                    id: +new Date() + Math.random(),
                                    text: lastWordChunk,
                                });
                            } else if (firstWordChunk && firstWordChunk.length) {
                                newContent.push({
                                    ...v,
                                    id: +new Date() + Math.random(),
                                    text: firstWordChunk,
                                });

                                newContent.push({
                                    ...v,
                                    id: +new Date() + Math.random(),
                                    text: sameLetters,
                                    [event]: selectedValue,
                                });
                            } else if (lastWordChunk && lastWordChunk.length) {
                                let newStyles = {};

                                if (v.background || event === 'background') {
                                    newStyles = {
                                        borderTopLeftRadius: 0,
                                        borderBottomLeftRadius: 0,
                                        paddingLeft: 0,
                                    }
                                }

                                newContent.push({
                                    ...v,
                                    id: +new Date() + Math.random(),
                                    text: sameLetters,
                                    [event]: selectedValue,
                                    ...newStyles
                                });

                                newContent.push({
                                    ...v,
                                    id: +new Date() + Math.random(),
                                    text: lastWordChunk,
                                });
                            } else {
                                newContent.push({
                                    ...v,
                                    id: +new Date() + Math.random(),
                                    [event]: selectedValue,
                                });
                            }
                        } else {
                            newContent.push({
                                ...v,
                            });
                        }
                    }
                } else {
                    newContent.push({
                        ...v,
                    });
                }
            } else {
                newContent.push({
                    ...v,
                });
            }
        });

        this.props.getUpdateContent([...newContent]);
    };

    handleTextSelect = (textSelected, startElementSelectedText, endElementSelectedText) => {
        console.log('handleTextSelect', textSelected);

        // let selected = this.props.values.findIndex(e => e.id === id);
        debugger;

        let selected = (startElementSelectedText === endElementSelectedText || startElementSelectedText < endElementSelectedText) ? startElementSelectedText : endElementSelectedText;

        this.setState({
            idElementSelected: selected,
        });

        console.log('selected ID:', selected);

        this.setState({ textSelected });
    };

    handleControlsButtonClick = (event, selectedValue = null) => {
        let newPieces = [];

        let approveTextSelected = (event !== 'br') ? window.confirm(`You the selected text: ${this.state.textSelected }`) : true;

        if (approveTextSelected && this.props.values && this.props.values.length) {
            let currentValue = [...this.props.values],
                selectedElement = currentValue[this.state.idElementSelected];
            debugger;

            let currentText = selectedElement.text;

            let oldText = '',
                newItem = {},
                lastText = '';

            if (event !== 'br') {
                this.modifyChunks(event, selectedValue);

                return;
            } else {
                let selectionStart = (this.state.selectionStart !== null) ? this.state.selectionStart : 0;
                debugger;
                oldText = currentText.slice(0, selectionStart);
                newItem = {...selectedElement, id: +new Date() + Math.random(), text: null, isEditing: false, [event]: selectedValue};
                lastText = currentText.slice(selectionStart, currentText.length);
            }

            if (oldText.trim()) {
                if (oldText === currentText) {
                    newPieces.push({...selectedElement, id: +new Date() + Math.random(), text: oldText, isEditing: false});

                    newPieces.push(newItem);
                    debugger;
                    newPieces.push({...selectedElement, id: +new Date() + Math.random(), text: '', isEditing: true});
                } else newPieces.push({...selectedElement, id: +new Date() + Math.random(), text: oldText});
            }

            newPieces.push(newItem);

            if (lastText.trim()) newPieces.push({
                ...selectedElement,
                id: +new Date() + Math.random(),
                text: lastText
            });

            let firstChunk = this.props.values.slice(0, this.state.idElementSelected),
                lastChunk = (this.state.idElementSelected !== 0) ? this.props.values.slice(this.state.idElementSelected + 1, this.props.values.length) : null;

            let newContent = [...firstChunk, ...newPieces];
                newContent = lastChunk ? newContent.concat([...lastChunk]) : newContent;

            this.props.getUpdateContent(newContent);

            console.log(newContent);
        }
    };

    handleCompileToJson = () => {
        if (this.props.values && this.props.values.length) {
            let content = [...this.props.values],
                someContentParams = [];

            for (let o = 0; o < content.length; o++ ) {
                if (content[o].text && content[o + 1] && content[o + 1].text) {
                    if ( (content[o].fontSize && content[o + 1].fontSize) && (content[o].fontSize === content[o + 1].fontSize) ) {
                        if ( content[o].color === content[o + 1].color && content[o].background === content[o + 1].background ) {
                            someContentParams.push({
                                ...content[o],
                                text: content[o].text + content[o + 1].text,
                            });

                            o++;
                        } else someContentParams.push({...content[o]});
                    } else someContentParams.push({...content[o]});
                } else if (content[o].text) someContentParams.push({...content[o]});
            }

            console.log('JSON result:', JSON.stringify(someContentParams), someContentParams);
        }
    };

    render() {
        console.log('TextEditor rendered', this.props, this.state);

        return (
            <div className={s.container}>
                <PanelControls onControlsButtonClick={this.handleControlsButtonClick} onCompileToJson={this.handleCompileToJson}/>

                <TextContent
                    values={this.props.values} onUpdateIdElementSelected={this.handleUpdateIdElementSelected}
                    textSelected={this.state.textSelected} onTextSelect={this.handleTextSelect}
                    onControlsButtonClick={this.handleControlsButtonClick} onUpdateSelectionStart={this.handleUpdateSelectionStart}
                />
            </div>
        );
    }
}

const mapStateToProps = state => ({
    values: state.content.values,
});

const mapDispatchToProps = dispatch => ({
    getUpdateContent: newContent => dispatch(getUpdateContent(newContent)),
});

export default connect(mapStateToProps, mapDispatchToProps)(TextEditor)
