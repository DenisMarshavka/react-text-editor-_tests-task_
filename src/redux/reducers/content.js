import {
    UPDATE_CHUNK,
    UPDATE_CONTENT,
} from "../actions/actionTypes"

const initialState = {
    values: [
        {id: +new Date() + Math.random(), text: 'he', isEditing: false, color: '#000', fontSize: 16},
        {id: +new Date() + Math.random(), text: 'llo', isEditing: false, color: '#fff', background: '#000', fontSize: 16},
        {id: +new Date() + Math.random(), text: ' 10 gu', isEditing: false, color: '#000', fontSize: 16},
        {id: +new Date() + Math.random(), text: 'ys', isEditing: false, color: '#acacac', background: 'red', fontSize: 16},
    ],
};

export default (state = initialState, action) => {
    switch (action.type) {
        case UPDATE_CONTENT:
            return {
                ...state,
                values: action.payload,
            };

        case UPDATE_CHUNK:
            return {
                ...state,
                values: action.payload
            };

        default:
            return {
                ...state,
            };
    }
};
