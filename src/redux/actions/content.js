import {
    UPDATE_CHUNK,
    UPDATE_CONTENT,
} from "./actionTypes"

export const getUpdateContent = newChunks => ({
    type: UPDATE_CONTENT,
    payload: newChunks
});

export const getUpdateChunk = payload => ({
    type: UPDATE_CHUNK,
    payload
});
